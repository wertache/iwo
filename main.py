from nltk import word_tokenize
from gutenberg.acquire import load_etext
from gutenberg.cleanup import strip_headers


book_title = str(input('title of the book '))
book_author = str(input('author of the book '))
book_author_sex = str(input('sex of the author '))
book_code = int(input('gutenberg\'s index of the book '))

try:
    book_text = strip_headers(load_etext(book_code))
except:
    print('Please enter a valid index')
else:
    book_text_tok = word_tokenize(book_text, 'english')

    book_text_tok_len = len(book_text_tok)
    fp_pn = 0
    tp_pn = 0

    for word in book_text_tok:
        if word.lower() in ['i', 'me','my','mine','myself','we','us','our','ours','ourselves']:
            fp_pn += 1
        if word.lower() in ['he','him','his','himself','she','her','hers','herself','they','them','their','theirs','themselves']:
            tp_pn += 1

    print("In this book there are: \n{0} first person pronouns \n{1} third person pronouns \n{2} total words.".format(fp_pn,tp_pn,book_text_tok_len))

    file = open('data.csv','a')

    file.write('{0},{1},{2},{3},{4},{5}\n'.format(book_title,book_author,book_author_sex,book_text_tok_len,fp_pn,tp_pn))

    file.close 